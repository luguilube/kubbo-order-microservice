FROM openjdk:11

VOLUME /tmp

ADD ./target/kubbo-order-microservice-0.0.1-SNAPSHOT.jar kubbo-order-microservice.jar

ENTRYPOINT ["java", "-jar", "/kubbo-order-microservice.jar"]