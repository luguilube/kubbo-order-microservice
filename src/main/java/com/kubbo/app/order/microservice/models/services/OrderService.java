package com.kubbo.app.order.microservice.models.services;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.kubbo.app.order.microservice.models.entities.Customer;
import com.kubbo.app.order.microservice.models.entities.Order;
import com.kubbo.app.order.microservice.models.entities.Product;
import com.kubbo.app.order.microservice.models.entities.Warehouse;

public interface OrderService {
	public List<Order> findAllByDeletedFalse();
	public Page<Order> findAllByDeletedFalse(Pageable pageable);
	public Order findById(Long id);
	public Order save(Order order);
	public boolean deleteById(Long id);
	public List<Warehouse> getWarehousesList();
	public List<Customer> getCustomersList();
	public List<Long> getProductsIdsByWarehouse(@PathVariable Long id);
	public Long getStockByWarehouseAndProduct(@PathVariable Long warehouseId, @PathVariable Long productId);
	public List<Product> getProductsByIdsList(@RequestBody List<Long> ids);
	public ResponseEntity<?> sotckUpdateAfterOrderPlaced(@RequestBody List<Map<String, Long>> stock, Long id);
	public void insertOrderDetail(Long orderId, Long productId, Long quantity);
}
