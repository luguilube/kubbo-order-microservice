package com.kubbo.app.order.microservice.clients;

import java.util.List;
import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.kubbo.app.order.microservice.models.entities.Warehouse;

@FeignClient(name = "kubbo-warehouse-microservice")
public interface WarehouseFeignClient {
	
	@GetMapping("/list")
	public List<Warehouse> list();
	
	@GetMapping("/get-products-ids-by-warehouse/{id}")
	public List<Long> getProductsIdsByWarehouse(@PathVariable Long id);
	
	@GetMapping("/get-stock-by-warehouse-and-product/{warehouseId}/{productId}")
	public Long getStockByWarehouseAndProduct(@PathVariable Long warehouseId, @PathVariable Long productId);
	
	@PutMapping("/sotck-update-after-order-placed/{id}")
	public ResponseEntity<?> sotckUpdateAfterOrderPlaced(@RequestBody List<Map<String, Long>> stock, @PathVariable Long id);
}
