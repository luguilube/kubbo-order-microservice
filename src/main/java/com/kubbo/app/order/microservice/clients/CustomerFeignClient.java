package com.kubbo.app.order.microservice.clients;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import com.kubbo.app.order.microservice.models.entities.Customer;

@FeignClient(name = "kubbo-customer-microservice")
public interface CustomerFeignClient {

	@GetMapping("/list")
	public List<Customer> list();
}
