package com.kubbo.app.order.microservice.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kubbo.app.order.microservice.models.entities.OrderStatus;
import com.kubbo.app.order.microservice.models.repositories.OrderStatusRepository;

@Service
public class OrderStatusServiceImpl implements OrderStatusService {

	@Autowired
	private OrderStatusRepository repository;
	
	@Override
	@Transactional(readOnly = true)
	public List<OrderStatus> findAllByDeletedFalse() {
		return repository.findAllByDeletedFalse();
	}

	@Override
	@Transactional(readOnly = true)
	public OrderStatus findByIdAndDeletedFalse(Long id) {
		return repository.findByIdAndDeletedFalse(id).orElse(null);
	}

}
