package com.kubbo.app.order.microservice.models.services;

import java.util.List;

import com.kubbo.app.order.microservice.models.entities.OrderStatus;

public interface OrderStatusService {
	public List<OrderStatus> findAllByDeletedFalse();
	public OrderStatus findByIdAndDeletedFalse(Long id);
}
