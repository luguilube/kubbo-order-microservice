package com.kubbo.app.order.microservice.models.services;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kubbo.app.order.microservice.clients.CustomerFeignClient;
import com.kubbo.app.order.microservice.clients.ProductFeignClient;
import com.kubbo.app.order.microservice.clients.WarehouseFeignClient;
import com.kubbo.app.order.microservice.models.entities.Customer;
import com.kubbo.app.order.microservice.models.entities.Order;
import com.kubbo.app.order.microservice.models.entities.Product;
import com.kubbo.app.order.microservice.models.entities.Warehouse;
import com.kubbo.app.order.microservice.models.repositories.OrderRepository;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderRepository repository;
	
	@Autowired
	private WarehouseFeignClient warehouseFeignClient;
	
	@Autowired
	private CustomerFeignClient customerFeignClient;
	
	@Autowired
	private ProductFeignClient productFeignClient;

	@Override
	@Transactional(readOnly = true)
	public List<Order> findAllByDeletedFalse() {
		return repository.findAllByDeletedFalse();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Order> findAllByDeletedFalse(Pageable pageable) {
		return repository.findAllByDeletedFalse(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Order findById(Long id) {
		return repository.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Order save(Order order) {
		return repository.save(order);
	}

	@Override
	@Transactional
	public boolean deleteById(Long id) {
		Optional<Order> op = repository.findById(id);

		if (!op.isEmpty()) {
			op.get().setDeleted(true);
			repository.save(op.get());

			return true;
		}

		return false;
	}

	@Override
	public List<Warehouse> getWarehousesList() {
		return warehouseFeignClient.list();
	}

	@Override
	public List<Customer> getCustomersList() {
		return customerFeignClient.list();
	}

	@Override
	public List<Long> getProductsIdsByWarehouse(Long id) {
		return warehouseFeignClient.getProductsIdsByWarehouse(id);
	}

	@Override
	public Long getStockByWarehouseAndProduct(Long warehouseId, Long productId) {
		return warehouseFeignClient.getStockByWarehouseAndProduct(warehouseId, productId);
	}

	@Override
	public List<Product> getProductsByIdsList(List<Long> ids) {
		return productFeignClient.getProductsByIdsList(ids);
	}

	@Override
	public ResponseEntity<?> sotckUpdateAfterOrderPlaced(List<Map<String, Long>> stock, Long id) {
		return warehouseFeignClient.sotckUpdateAfterOrderPlaced(stock, id);
	}

	@Override
	@Transactional
	public void insertOrderDetail(Long orderId, Long productId, Long quantity) {
		repository.insertOrderDetail(orderId, productId, quantity);
	}

}
