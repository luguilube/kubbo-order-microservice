package com.kubbo.app.order.microservice.clients;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.kubbo.app.order.microservice.models.entities.Product;

@FeignClient(name = "kubbo-product-microservice")
public interface ProductFeignClient {
	
	@PostMapping("/get-products-by-ids-list")
	public List<Product> getProductsByIdsList(@RequestBody List<Long> ids);
}
