package com.kubbo.app.order.microservice.models.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.kubbo.app.order.microservice.models.entities.Order;

public interface OrderRepository extends PagingAndSortingRepository<Order, Long> {
	public List<Order> findAllByDeletedFalse();
	public Page<Order> findAllByDeletedFalse(Pageable pageable);
	public Optional<Order> findById(Long id);
	
	@Modifying(clearAutomatically = true)
	@Query(value = "INSERT INTO order_details (order_id, product_id, quantity) VALUES (:order_id, :productId, :quantity)", nativeQuery = true)
	public void insertOrderDetail(@Param("order_id") Long order_id, Long productId, Long quantity);
}
