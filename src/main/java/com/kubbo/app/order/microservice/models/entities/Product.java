package com.kubbo.app.order.microservice.models.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sun.istack.NotNull;

@Entity
@Table(name = "products")
public class Product {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty
	private String sku;
	
	@NotEmpty
	private String name;
	
	private String description;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date createdAt;
	
	@NotNull
	private Boolean enabled;

	@Column(columnDefinition = "boolean default false")
	private Boolean deleted;
	
	@JsonIgnoreProperties(value = {"product", "hibernateLazyInitializer", "handler"}, allowSetters = true)
	@OneToMany(mappedBy = "product", fetch=FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<OrderDetail> orderDetail;
	
	@PrePersist
	private void prePersist() {
		createdAt = new Date();
		deleted = deleted == null ? false : deleted;
	}
	
	public Product() {
		orderDetail = new HashSet<>();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	
	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
	
	public Set<OrderDetail> getOrderDetail() {
		return orderDetail;
	}

	public void setOrderDetail(Set<OrderDetail> orderDetail) {
		this.orderDetail = orderDetail;
	}
	
	public void addOrderDetail(OrderDetail orderDetail) {
		this.orderDetail.add(orderDetail);
	}
	
	public void removeOrderDetail(OrderDetail orderDetail) {
		this.orderDetail.remove(orderDetail);
	}

	@Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", sku='" + sku + "'" +
                ", name='" + name + "'" +
                ", description='" + description + "'" +
                ", createdAt='" + createdAt + "'" +
                ", enabled=" + enabled + 
                ", deleted=" + deleted + 
                '}';
    }
}
