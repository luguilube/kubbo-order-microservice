package com.kubbo.app.order.microservice.models.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.kubbo.app.order.microservice.models.entities.OrderStatus;

public interface OrderStatusRepository extends CrudRepository<OrderStatus, Long> {
	public List<OrderStatus> findAllByDeletedFalse();
	public Optional<OrderStatus> findByIdAndDeletedFalse(Long id);
}
