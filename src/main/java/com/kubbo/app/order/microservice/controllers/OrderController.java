package com.kubbo.app.order.microservice.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kubbo.app.order.microservice.models.entities.Order;
import com.kubbo.app.order.microservice.models.entities.OrderDetail;
import com.kubbo.app.order.microservice.models.entities.OrderStatus;
import com.kubbo.app.order.microservice.models.entities.Product;
import com.kubbo.app.order.microservice.models.services.OrderService;
import com.kubbo.app.order.microservice.models.services.OrderStatusService;

@RestController
public class OrderController {
	private static final Logger log = LoggerFactory.getLogger(OrderController.class);
	
	private final OrderService service;
	private final OrderStatusService orderStatusService;

	@Autowired
	public OrderController(OrderService service, OrderStatusService orderStatusService) {
		this.service = service;
		this.orderStatusService = orderStatusService;
	}
	
	@GetMapping("/list")
	public ResponseEntity<?> list() {
		try {
            return ResponseEntity.ok().body(service.findAllByDeletedFalse());
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
	}
	
	@GetMapping("/paginated-list")
    public ResponseEntity<?> paginatedList(Pageable pageable) {
        try {
            return ResponseEntity.ok().body(service.findAllByDeletedFalse(pageable));
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
    }

    @PostMapping("store")
    public ResponseEntity<?> store(@Valid @RequestBody Order order, BindingResult result) {
        try {
            if (result.hasErrors()) {
                return validation(result);
            }
            
            List<Map<String, Long>> stockList = new ArrayList<>();
            Map<String, Long> stock = new HashMap<>();
            
            for (OrderDetail od : order.getOrderDetail()) {
            	stock = new HashMap<>();
            	stock.put("productId", od.getProduct().getId());
            	stock.put("quantity", od.getQuantity());
            	
            	stockList.add(stock);
            }
            
            this.service.sotckUpdateAfterOrderPlaced(stockList, order.getWarehouse().getId());
            
            Order newOrder = new Order();
            newOrder.setCustomer(order.getCustomer());
            newOrder.setWarehouse(order.getWarehouse());
            
            List<OrderStatus> status = this.orderStatusService.findAllByDeletedFalse();
            newOrder.setStatus(status.get(0));
            newOrder = service.save(newOrder);
           
            for (OrderDetail od : order.getOrderDetail()) {
            	service.insertOrderDetail(newOrder.getId(), od.getProduct().getId(), od.getQuantity());
            }
            	
            return ResponseEntity.status(HttpStatus.CREATED).body(newOrder);
        } catch (InternalError e) {
        	Map<String, String> error = new HashMap<>();
            error.put("error", e.getMessage());
        	log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
        } catch (Exception e1) {
        	log.error("Exception {}", e1);
            
        	Map<String, String> error = new HashMap<>();
            error.put("error", e1.getMessage());
            
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(error);
        }
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<?> get(@PathVariable Long id) {
        try {
            Order order = service.findById(id);

            if (order == null) {
                return ResponseEntity.notFound().build();
            }

            return ResponseEntity.ok().body(order);
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        try {
        	return service.deleteById(id) 
            		? ResponseEntity.noContent().build() 
    				: ResponseEntity.notFound().build();
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
    }
    
    @GetMapping("/get-status-list")
	public ResponseEntity<?> getStatusList() {
		try {
            return ResponseEntity.ok().body(orderStatusService.findAllByDeletedFalse());
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
	}
    
    @GetMapping("/get-warehouses-list")
	public ResponseEntity<?> getWarehousesList() {
		try {
            return ResponseEntity.ok().body(service.getWarehousesList());
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
	}
    
    @GetMapping("/get-products-by-warehouse/{id}")
    public ResponseEntity<?> getProductsByWarehouse(@PathVariable Long id) {
        try {
            List<Long> productsIds = service.getProductsIdsByWarehouse(id);

            if (productsIds.size() <= 0) {
                return ResponseEntity.notFound().build();
            }
            
            List<Product> products = service.getProductsByIdsList(productsIds);
            
            if (products.size() <= 0) {
                return ResponseEntity.notFound().build();
            }

            return ResponseEntity.ok().body(products);
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
    }

    @GetMapping("/get-customers-list")
	public ResponseEntity<?> getCustomersList() {
		try {
            return ResponseEntity.ok().body(service.getCustomersList());
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
	}
    
    @GetMapping("/get-stock-from-warehouse-for-product/{warehoyseId}/{productId}")
    public ResponseEntity<?> getStockFromWarehouseForOroduct(@PathVariable Long warehoyseId, @PathVariable Long productId) {
        try {
            return ResponseEntity.ok().body(service.getStockByWarehouseAndProduct(warehoyseId, productId));
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
    }
    
    @PutMapping("/status-update/{orderId}/{statusId}")
    public ResponseEntity<?> statusUpdate(@PathVariable Long orderId, @PathVariable Long statusId) {
        try {
            Order order = service.findById(orderId);
            
            if (order == null) {
            	return ResponseEntity.noContent().build();
            }
            
            OrderStatus status = orderStatusService.findByIdAndDeletedFalse(statusId);
            
            if (status == null) {
            	return ResponseEntity.noContent().build();
            }
            
            order.setStatus(status);
            
            if (status.getId() == 2) {
            	order.setShippingDate(new Date());
            } else {
            	if (status.getId() == 4 && order.getDeliveryDate() == null) {
            		order.setDeliveryDate(new Date());
            	}
            }
            
            return ResponseEntity.status(HttpStatus.CREATED).body(service.save(order));
        } catch (InternalError e) {
        	Map<String, String> error = new HashMap<>();
            error.put("error", e.getMessage());
        	log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
        } catch (Exception e1) {
        	log.error("Exception {}", e1);
            
        	Map<String, String> error = new HashMap<>();
            error.put("error", e1.getMessage());
            
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(error);
        }
    }

    
    private ResponseEntity<?> validation(BindingResult result) {
        Map<String, Object> errors = new HashMap<>();

        result.getFieldErrors().forEach(err -> {
            errors.put(err.getField(), "El campo " + err.getField() + " " + err.getDefaultMessage());
        });

        return ResponseEntity.badRequest().body(errors);
    }
    
}
